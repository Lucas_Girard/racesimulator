﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Data;

namespace RaceSimulator.Converter
{
    public class FlagConverter : IValueConverter
    {
       
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            
            //string urlImage = Path.Combine(AssemblyDirectory, @"Datas\\Flags\\" + value + ".bmp");
            string urlImage = Path.Combine(
                Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)
                , @"Datas\Flags\" + value + ".bmp");
            return urlImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
