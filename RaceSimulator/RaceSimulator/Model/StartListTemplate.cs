﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using SwissTiming.Timing.AcquisitionSimulator;

namespace RaceSimulator.Model
{
    public class StartListTemplate : ObservableObject
    {
        
        public int Bib { get; set; }
        public string Id { get; set; }
        public string Country { get; set; }
        public string Name { get; set; }

       
    }
}
