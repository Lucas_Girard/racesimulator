﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using GalaSoft.MvvmLight;

namespace RaceSimulator.Model
{
    [XmlRoot(ElementName = "Person")]
    public class Person : ObservableObject
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "country")]
        public string Country { get; set; }

    }
    [XmlRoot(ElementName = "EntryList")]
    public class EntryList : ObservableObject
    {
        [XmlElement(ElementName = "Person")]
        public List<Person> Person { get; set; }
    }
}
