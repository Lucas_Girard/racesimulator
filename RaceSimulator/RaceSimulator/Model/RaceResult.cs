﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using GalaSoft.MvvmLight;

namespace RaceSimulator.Model
{
    [XmlRoot(ElementName = "Competitors")]
    public class RaceResult : ObservableObject
    {
      [XmlElement("Rank")]
        public string Rank { get; set; }
        [XmlElement("Bib")]
        public int Bib { get; set; }
        [XmlElement("Country")]
        public string Country { get; set; }
        [XmlElement("Name")]
        public string Name { get; set; }
        [XmlElement("Time_IRM")]
        public string Result { get; set; }
        //public string Reason { get; set; }

        //public int ReactionTime { get; set; }
         public DateTime StartTime { get; set; }
    }
}
