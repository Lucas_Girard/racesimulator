﻿using System;using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RaceSimulator.Helpers
{
    public class NumberComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            double x1 = Convert.ToDouble(x.TrimEnd('s'));
            double y1 = Convert.ToDouble(y.TrimEnd('s'));
            if (x1 > y1)
                return 1;
            if (x1 < y1)
                return -1;
            return 0;
        }
    }
}
