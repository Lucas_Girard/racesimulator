﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace RaceSimulator.Helpers
{
    public class CustomSorter : IComparer<string>
    {
        private readonly IComparer<string> _baseComparer;
        public CustomSorter(IComparer<string> baseComparer)
        {
            _baseComparer = baseComparer;
        }

        public  int Compare(string x, string y)
        {
            
            if (_baseComparer.Compare(x, y) == 0)
                return 0;

            // "b" comes before everything else
            if (_baseComparer.Compare(x, "DNF") == 0)
                return -1;
            if (_baseComparer.Compare(y, "DNF") == 0)
                return 1;

            // "c" comes next
            if (_baseComparer.Compare(x, "DSQ") == 0)
                return -1;
            if (_baseComparer.Compare(y, "DSQ") == 0)
                return 1;
            if (_baseComparer.Compare(x, "DNS") == 0)
                return 1;
            return _baseComparer.Compare(y, "DNS") == 0 ? 1 : _baseComparer.Compare(x, y);
        }

       
    }
}
