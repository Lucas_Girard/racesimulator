﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using RaceSimulator.Model;

namespace RaceSimulator.ViewModel
{
    public class EditViewModel : ViewModelBase, IDisposable
    {
        #region PrivateProperties

        private string _name;
        private string _rank;
        private int _bib;
        private ObservableCollection<string> _listIrm;
        private string _selectedIrm;
        private RaceResult _raceResult;
        #endregion

        #region PublicProperties

        public string SelectedIrm
        {
            get
            {
                return _selectedIrm;
                
            }
            set
            {
                _selectedIrm = value;
                RaisePropertyChanged(() => SelectedIrm);
            }
        }
        public int Bib
        {
            get
            {
                return _bib;
                
            }
            set
            {
                _bib = value;
                RaisePropertyChanged(() => Bib);
            }
        }
        public string Rank
        {
            get { return _rank; }
            set
            {
                _rank = value;
                RaisePropertyChanged(() => Rank);
            }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public ObservableCollection<string> ListIrm
        {
            get { return _listIrm; }
            set
            {
                _listIrm = value;
                RaisePropertyChanged(() => ListIrm);
            }
        }



        public ICommand SubmitCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        #endregion

        #region Constructor

        public EditViewModel()
        {
            MessengerInstance.Register<NotificationMessage<RaceResult>>(this, CompToEditMethod);
            ListIrm = new ObservableCollection<string> {"DNF", "DSQ" , "DNS"};
            SubmitCommand = new RelayCommand(SubmitMethod);
            CancelCommand = new RelayCommand(CancelMethod);
        }



        #endregion


        #region UIEvents



        #endregion

        #region Methods
        private void CancelMethod()
        {
            MessengerInstance.Send(new NotificationMessage("CloseView","CloseEditView"));
        }

        private void SubmitMethod()
        {
            _raceResult.Result = _selectedIrm;
            MessengerInstance.Send<NotificationMessage<RaceResult>>(new NotificationMessage<RaceResult>(_raceResult,"CompetitorEdited"));
        }
        private void CompToEditMethod(NotificationMessage<RaceResult> obj)
        {
            if (obj.Notification == "SendToEdit")
            {
                _raceResult = obj.Content;
                _rank = _raceResult.Rank;
                _bib = _raceResult.Bib;
                _name = _raceResult.Name;
                RaisePropertyChanged(() => Name);
                RaisePropertyChanged(() => Bib);
                RaisePropertyChanged(() => Rank);
            }
           


        }

        #endregion

        #region IDisposable
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
               MessengerInstance.Unregister<NotificationMessage<RaceResult>>(this, CompToEditMethod);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~EditViewModel()
        {
            Dispose(false);
        }
        #endregion


    }
}
