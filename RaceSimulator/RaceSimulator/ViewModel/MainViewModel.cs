﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml.Serialization;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Win32;
using RaceSimulator.Helpers;
using RaceSimulator.Model;
using SwissTiming.Timing.AcquisitionSimulator;

namespace RaceSimulator.ViewModel
{
    public class MainViewModel : ViewModelBase, IDisposable
    {
        #region PrivateProperties

        //list
       
        private ObservableCollection<Person> _listPerson;
        private ObservableCollection<RaceResult> _listForXml;
        private List<Person> _listPersonForStartList;
        private List<Competitor> _listCompetitors;
        private ObservableCollection<StartListTemplate> _listDataGrid;
        private ObservableCollection<RaceResult> _listFinishDataGrid;
        private ObservableCollection<RaceResult> _tempList;
        private ObservableCollection<RaceResult> _listDisqualified;

        private Simulator _simulator;
        private RaceResult _selectedCompetitor;
        private DateTime _startTime;
        private int _count = 0;
        private int _startCount = 0;
        private int _competitors = 0;
        private string _country;
        private string _rank;
        private string _name;
        private string _time;
        private string _raceUpdate;

        private string _currentLanguage = "En";
        //booleans
        private bool _isGridResultIsVisible;
        private bool _isButtonAddIsEnable;
        private bool _isButtonStartIsEnable;
        private bool _editButtonIsVisible;
        private bool _isEditViewIsVisible;
        private bool _buttonSaveXmlEnable;
        private bool _mainCanvasIsEnable = true;
        private bool _isMassStartChecked = true;
        private bool _isLoadButtonEnable = true;
        private bool _isIndivStartChecked;
        private bool _radioButtonAreEnable ;
        private bool _isRestartEnable;

        //ViewModel
        private ViewModelBase _editCompetitorView;


        #endregion

        #region PublicProperties

        public bool IsRestartEnable
        {
            get { return _isRestartEnable; }
            set
            {
                _isRestartEnable = value;
                RaisePropertyChanged(() => IsRestartEnable);
            }
        }
        public bool RadioButtonAreEnable
        {
            get { return _radioButtonAreEnable; }
            set
            {
                _radioButtonAreEnable = value;
                RaisePropertyChanged(() => RadioButtonAreEnable);
            }
        }
        public bool IsIndivStartChecked
        {
            get { return _isIndivStartChecked; }
            set
            {
                _isIndivStartChecked = value;
                RaisePropertyChanged(() => IsIndivStartChecked);
            }
        }
        public bool IsMassStartChecked
        {
            get { return _isMassStartChecked; }
            set
            {
                _isMassStartChecked = value;
                RaisePropertyChanged(() => IsMassStartChecked);
            }
        }
        public string RaceUpdate
        {
            get { return _raceUpdate; }
            set
            {
                _raceUpdate = value;
                RaisePropertyChanged(() => RaceUpdate);
            }
        }
        public bool IsLoadButtonEnable
        {
            get { return _isLoadButtonEnable; }
            set
            {
                _isLoadButtonEnable = value;
                RaisePropertyChanged(() => IsLoadButtonEnable);
            }
        }
        public string Time
        {
            get { return _time; }
            set
            {
                _time = value;
                RaisePropertyChanged(() => Time);
            }
        }
        public string Rank
        {
            get { return _rank; }
            set
            {
                _rank = value;
                RaisePropertyChanged(() => Rank);
            }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged(() => Name);
            }
        }
        public string Country
        {
            get { return _country; }
            set
            {
                _country = value;
                RaisePropertyChanged(() => Country);
            }
        }
        public bool MainCanvasIsEnable
        {
            get { return _mainCanvasIsEnable; }
            set
            {
                _mainCanvasIsEnable = value;
                RaisePropertyChanged(() => MainCanvasIsEnable);
            }
        }
        public bool ButtonSaveXmlEnable
        {
            get { return _buttonSaveXmlEnable; }
            set
            {
                _buttonSaveXmlEnable = value;
                RaisePropertyChanged(() => ButtonSaveXmlEnable);
            }
        }
        public bool EditButtonIsVisible
        {
            get { return _editButtonIsVisible; }
            set
            {
                _editButtonIsVisible = value;
                RaisePropertyChanged(() => EditButtonIsVisible);
            }
        }
        public RaceResult SelectedCompetitor
        {
            get { return _selectedCompetitor; }
            set
            {
                _selectedCompetitor = value;
                RaisePropertyChanged(() => SelectedCompetitor);
            }
        }
        public ViewModelBase EditCompetitorView
        {
            get { return _editCompetitorView; }
            set
            {
                _editCompetitorView = value;
                RaisePropertyChanged(() => EditCompetitorView);
            }
        }
        public bool IsEditViewIsVisible
        {
            get { return _isEditViewIsVisible; }
            set
            {
                _isEditViewIsVisible = value;
                RaisePropertyChanged(() => IsEditViewIsVisible);
            }
        }
        public bool IsButtonStartIsEnable
        {
            get { return _isButtonStartIsEnable; }
            set
            {
                _isButtonStartIsEnable = value;
                RaisePropertyChanged(() => IsButtonStartIsEnable);
            }
        }
        public bool IsButtonAddIsEnable
        {
            get { return _isButtonAddIsEnable; }
            set
            {
                _isButtonAddIsEnable = value;
                RaisePropertyChanged(() => IsButtonAddIsEnable);
            }
        }
       

        public bool IsGridResultIsVisible
        {
            get { return _isGridResultIsVisible; }
            set
            {
                _isGridResultIsVisible = value;
                RaisePropertyChanged(() => IsGridResultIsVisible);
            }
        }

        public ObservableCollection<RaceResult> ListDisqualified
        {
            get { return _listDisqualified; }
            set
            {
                _listDisqualified = value;
                RaisePropertyChanged(() => ListDisqualified);
            }
        }
        public ObservableCollection<RaceResult> ListFinishDataGrid
        {
            get { return _listFinishDataGrid; }
            set
            {
                _listFinishDataGrid = value;
                RaisePropertyChanged(() => ListFinishDataGrid);
            }
        }

        public ObservableCollection<StartListTemplate> ListDataGrid
        {
            get { return _listDataGrid; }
            set
            {
                _listDataGrid = value;
                RaisePropertyChanged(() => ListDataGrid);
            }
        }

        public List<Competitor> ListCompetitors
        {
            get { return _listCompetitors; }
            set
            {
                _listCompetitors = value;
                RaisePropertyChanged(() => ListCompetitors);
            }
        }

        public ObservableCollection<Person> ListPerson
        {
            get { return _listPerson; }
            set
            {
                _listPerson = value;
                RaisePropertyChanged(() => ListPerson);
            }
        }

        public SynchronizationContext ViewContext { get; set; }

       

        #endregion

        #region ICommand

        //Command for swith language french
        public ICommand ChangeForFrenchLanguage { get; private set; }

        //Command for swith language english
        public ICommand ChangeForEnglishLanguage { get; private set; }

        //Command for loading button
        public ICommand LoadPerson { get; private set; }

        //Command for create start list button
        public ICommand CreateStartList { get; private set; }

        //Command for remove startList
        public ICommand RemoveStartList { get; private set; }

        //Command for start simulation
        public ICommand StartCommand { get; private set; }
        //Command for Save to Xml Button
        public ICommand SaveToXmlCommand { get; private set; }
        //Command for Edit button in listview
        public ICommand EditButtonCommand { get; private set; }

        //Command for button restart
        public ICommand RestartCommand { get; private set; }

        #endregion

        #region Constructor

        public MainViewModel()
        {
            Country = "Country";
            Name = "Name";
            Rank = "Rank";
            Time = "Time | IRM";
          
            ChangeForEnglishLanguage = new RelayCommand(ChangeForEnglish);
            ChangeForFrenchLanguage = new RelayCommand(ChangeForFrench);
            LoadPerson = new RelayCommand(LoadData);
            CreateStartList = new RelayCommand(CreateStartListMethod);
            StartCommand = new RelayCommand(StartSimulationMethod);
            RemoveStartList = new RelayCommand(RemoveStartListMethod);
            SaveToXmlCommand = new RelayCommand(SaveToXmlMethod);
            EditButtonCommand = new RelayCommand<object>(EditButtonMethod);
            RestartCommand = new RelayCommand(RestartMethod);

           
            MessengerInstance.Register<NotificationMessage<RaceResult>>(this, CompEditedMethod);
            MessengerInstance.Register<NotificationMessage>(this,GetMessagesFromOthersVm);

           
        }



        #endregion


        #region Methods
        /// <summary>
        /// RestartMethod : Method to restart application to initial state
        /// </summary>
        private void RestartMethod()
        {
            _competitors = _count = _startCount = 0;
            _listFinishDataGrid.Clear();
            _listDataGrid.Clear();
            _listDisqualified.Clear();
            _tempList.Clear();
            _isGridResultIsVisible = false;
            RaisePropertyChanged(() => IsGridResultIsVisible);
            _buttonSaveXmlEnable = false;
            RaisePropertyChanged(() => ButtonSaveXmlEnable);
            _isButtonStartIsEnable = false;
            RaisePropertyChanged(() => IsButtonStartIsEnable);
            _radioButtonAreEnable = false;
            RaisePropertyChanged(() => RadioButtonAreEnable);
            _isRestartEnable = false;
            RaisePropertyChanged(() => IsRestartEnable);
            _editButtonIsVisible = false;
            RaisePropertyChanged(() => EditButtonIsVisible);
            LoadData();
        }
        /// <summary>
        /// GetMessagesFromOthersVm : Method to get simple message notification from others viewmodel
        /// </summary>
        /// <param name="obj"></param>
        private void GetMessagesFromOthersVm(NotificationMessage obj)
        {
            switch (obj.Notification)
            {
                case "CloseEditView":
                    _isEditViewIsVisible = false;
                    _mainCanvasIsEnable = true;
                    RaisePropertyChanged(() => MainCanvasIsEnable);
                    RaisePropertyChanged(() => IsEditViewIsVisible);
                    break;
            }
        }

        /// <summary>
        /// LoadData : Method to load data from xml file
        /// </summary>
        public void LoadData()
        {
            var deserializer = new XmlSerializer(typeof(EntryList));
            ListPerson = new ObservableCollection<Person>();
            var fileName = Path.Combine(
                Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)
                , @"Datas\entrylist.xml");
            
            using (FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                var list = (EntryList) deserializer.Deserialize(fs);
                foreach (var person in list.Person)
                {
                    ListPerson.Add(person);
                }
            }

            _isButtonAddIsEnable = true;
            _isLoadButtonEnable = false;
            RaisePropertyChanged(() => IsLoadButtonEnable);
            RaisePropertyChanged(() => IsButtonAddIsEnable);
            RaisePropertyChanged(() => ListPerson);
        }

        /// <summary>
        /// ChangeForFrench : Method to switch interface language for french
        /// </summary>
        private void ChangeForFrench()
        {
            _currentLanguage = "Fr";
            Uri langFileFrench = new Uri("Languages/InterfaceLanguage_fr.xaml", UriKind.Relative);
            ResourceDictionary rd = Application.LoadComponent(langFileFrench) as ResourceDictionary;
            Application.Current.Resources.MergedDictionaries.Clear();
            Application.Current.Resources.MergedDictionaries.Add(rd);
            _country = "Pays";
            RaisePropertyChanged(() => Country);
            _name = "Nom";
            RaisePropertyChanged(() => Name);
            _rank = "Rang";
            RaisePropertyChanged(() => Rank);
            _time = "Temps | IRM";
            RaisePropertyChanged(() => Time);
        }

        /// <summary>
        /// ChangeForEnglish : Method to swith interface language for english
        /// </summary>
        private void ChangeForEnglish()
        {
            _currentLanguage = "En";
            Uri langFileFrench = new Uri("Languages/InterfaceLanguage_en.xaml", UriKind.Relative);
            ResourceDictionary rd = Application.LoadComponent(langFileFrench) as ResourceDictionary;
            Application.Current.Resources.MergedDictionaries.Clear();
            Application.Current.Resources.MergedDictionaries.Add(rd);
            _country = "Country";
            _name = "Name";
            _rank = "Rank";
            _time = "Time | IRM";
            RaisePropertyChanged(() => Time);
            RaisePropertyChanged(() => Rank);
            RaisePropertyChanged(() => Country);
            RaisePropertyChanged(() => Name);
        }

        /// <summary>
        /// CreateStartListMethod : Method to generate a start list from a list of person
        /// </summary>
        private void CreateStartListMethod()
        {
            _listCompetitors = new List<Competitor>();
            _listPersonForStartList = new List<Person>();
            _listPersonForStartList = ShuffleUp(_listPerson);
            _listDataGrid = new ObservableCollection<StartListTemplate>();

            for (int i = 0; i < _listPersonForStartList.Count; i++)
            {
                var competitor = new Competitor(_listPersonForStartList.ElementAt(i).Name, i + 1);
                _listCompetitors.Add(competitor);
                var starlistTemplate = new StartListTemplate
                {
                    Bib = i + 1,
                    Country = _listPersonForStartList.ElementAt(i).Country,
                    Id = _listPersonForStartList.ElementAt(i).Id,
                    Name = _listPersonForStartList.ElementAt(i).Name
                };
                _listDataGrid.Add(starlistTemplate);
            }
          

            RaisePropertyChanged(() => ListDataGrid);
            _isButtonStartIsEnable = true;
            RaisePropertyChanged(() => IsButtonStartIsEnable);
            _radioButtonAreEnable = true;
            RaisePropertyChanged(() => RadioButtonAreEnable);


        }
        /// <summary>
        /// RemoveStartListMethod : Method to remove the startList
        /// </summary>
        private void RemoveStartListMethod()
        {
            _isLoadButtonEnable = true;
            RaisePropertyChanged(() => IsLoadButtonEnable);
            _isButtonStartIsEnable = false;
            RaisePropertyChanged(() => IsButtonStartIsEnable);
            _listDataGrid.Clear();
            LoadData();
        }


        //}
        /// <summary>
        /// ShuffleUp : Method to shuffle the person list and get 10 of them for the start list
        /// </summary>
        /// <param name="persons"></param>
        /// <returns></returns>
        public List<Person> ShuffleUp(ObservableCollection<Person> persons)
        {
            var rdm = new Random();
            var list = new List<Person>();

            while (persons.Count > 0)
            {
                var randomIndex = rdm.Next(0, persons.Count);
                list.Add(persons[randomIndex]);
                persons.RemoveAt(randomIndex);
            }
            list = list.Take(10).ToList();

            return list;
        }
        /// <summary>
        /// EditButtonMethod : method to edit competitor after the end of the race
        /// </summary>
        private void EditButtonMethod(object obj)
        {
            _isEditViewIsVisible = true;
             RaisePropertyChanged(() => IsEditViewIsVisible);
            _editCompetitorView = new EditViewModel();
            RaisePropertyChanged(() => EditCompetitorView);
            _mainCanvasIsEnable = false;
            RaisePropertyChanged(() => MainCanvasIsEnable);
            MessengerInstance.Send<NotificationMessage<RaceResult>>(new NotificationMessage<RaceResult>(obj as RaceResult,"SendToEdit" ));
        }

        /// <summary>
        /// StartSimulationMethod : Method to start simulation from SwissTiming.dll
        /// </summary>
        private void StartSimulationMethod()
        {
            _isButtonAddIsEnable = false;
            _isButtonStartIsEnable = false;
            _isGridResultIsVisible = true;
            _raceUpdate = _currentLanguage == "Fr" ? " la Course vient de démarrer" : " the Race has just started";
            _radioButtonAreEnable = false;
            RaisePropertyChanged(() => IsButtonAddIsEnable);
            RaisePropertyChanged(() => RaceUpdate);
            RaisePropertyChanged(() => IsGridResultIsVisible);
            RaisePropertyChanged(() => IsButtonStartIsEnable);
            RaisePropertyChanged(() => IsLoadButtonEnable);
            RaisePropertyChanged(() => RadioButtonAreEnable);
            _listFinishDataGrid = new ObservableCollection<RaceResult>();
            _listDisqualified = new ObservableCollection<RaceResult>();
            _tempList = new ObservableCollection<RaceResult>();
            _simulator = _isIndivStartChecked ? new Simulator(StartKind.Individual) : new Simulator(StartKind.MassStart);
            _simulator.Initialize(_listCompetitors);
            _simulator.RaceCompleted += SimulatorOnRaceCompleted;
            _simulator.RaceStarted += SimulatorOnRaceStarted;
            _simulator.CompetitorChanged += SimulatorOnCompetitorChanged;
            _simulator.Start();

        }

        /// <summary>
        /// SimulatorOnCompetitorChanged : Method to get all the competitor changed events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="competitorChangedEventArgs"></param>
        private void SimulatorOnCompetitorChanged(object sender, CompetitorChangedEventArgs competitorChangedEventArgs)
        {
            if (competitorChangedEventArgs != null)
            {
                _competitors++;
                RaceResult comp2 = new RaceResult();
                var compInfo2 = _listDataGrid.First(p => p.Bib == competitorChangedEventArgs.Bib);
                comp2.Bib = competitorChangedEventArgs.Bib;
                //compInfo2 = _listDataGrid.First(p => p.Bib == bib);
                comp2.Name = compInfo2.Name;
                comp2.Country = compInfo2.Country;
                comp2.Result = competitorChangedEventArgs.Irm.ToString();
                //comp2.RaceFinished = "Unranked";
                comp2.Rank = "--";
                Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                {
                    if (IsIndivStartChecked)
                    {
                        _listFinishDataGrid.Remove(comp2);
                        _listFinishDataGrid = _tempList;
                        RaisePropertyChanged(() => ListFinishDataGrid);
                        UpdateRank();
                      

                    }
                    _listDisqualified.Add(comp2);
                    if (_listDisqualified.Count > 0)
                    {
                        _listDisqualified = new ObservableCollection<RaceResult>(_listDisqualified.OrderBy(p => p.Result, new CustomSorter(StringComparer.CurrentCulture))); ;
                    }
                    RaisePropertyChanged(() => ListDisqualified);

                    if (_competitors == 10)
                    {
                        RaceIsOverMethod();
                    }
                }));
                
            }
        }

        /// <summary>
        /// SimulatorOnRaceStarted : Method to get all the race started events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="raceStartedEventArgs"></param>
        private void SimulatorOnRaceStarted(object sender, RaceStartedEventArgs raceStartedEventArgs)
        {
            if (raceStartedEventArgs != null)
            {
                if (IsMassStartChecked)
                {
                     _startTime = raceStartedEventArgs.EventTime;

                }
                else
                {
                    RaceResult comp = new RaceResult();
                    var compInfo = _listDataGrid.First(p => p.Bib == raceStartedEventArgs.Bib);
                    comp.Bib = raceStartedEventArgs.Bib;
                    comp.Rank = (_startCount + 1).ToString();
                    comp.Name = compInfo.Name;
                    comp.Result = "--";
                    comp.Country = compInfo.Country;
                    comp.StartTime = raceStartedEventArgs.EventTime;
                    Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                    {
                       
                        _listFinishDataGrid.Add(comp);
                        RaisePropertyChanged(() => ListFinishDataGrid);

                    }));
                    _startCount++;
                   
                }
            }

        }

        /// <summary>
        /// SimulatorOnRaceCompleted : Method to get all the race completed events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="raceCompletedEventArgs"></param>
        private void SimulatorOnRaceCompleted(object sender, RaceCompletedEventArgs raceCompletedEventArgs)
        {
            if (raceCompletedEventArgs != null)
            {
                if (IsMassStartChecked)
                {
                    _competitors++;
                    RaceResult comp = new RaceResult();
                    var compInfo = _listDataGrid.First(p => p.Bib == raceCompletedEventArgs.Bib);
                    comp.Bib = raceCompletedEventArgs.Bib;
                    comp.Rank = (_count + 1).ToString();
                    comp.Name = compInfo.Name;
                    comp.Country = compInfo.Country;
                    TimeSpan ts = raceCompletedEventArgs.EventTime.Subtract(_startTime);
                    var result = ts.TotalSeconds;
                    result = Math.Round(result, 1);
                    comp.Result = result + "s";
                    _count++;
                    Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        _listFinishDataGrid.Add(comp);
                        RaisePropertyChanged(() => ListFinishDataGrid);
                        if (_competitors == 10)
                        {
                            RaceIsOverMethod();
                        }

                    }));
                   
                }
                else
                {
                    _competitors++;
                    var compInfo = _listFinishDataGrid.First(p => p.Bib == raceCompletedEventArgs.Bib);
                   
                    TimeSpan ts = raceCompletedEventArgs.EventTime.Subtract(compInfo.StartTime);
                    var result = ts.TotalSeconds;
                    result = Math.Round(result, 1);
                    compInfo.Result = result + "s";
                   
                    Application.Current.Dispatcher.BeginInvoke((Action) (() =>
                    {
                        _listFinishDataGrid.Remove(compInfo);
                        UpdateRank();
                        _tempList.Add(compInfo);
                        if (_tempList.Count > 0)
                           _tempList = new ObservableCollection<RaceResult>(_tempList
                                .OrderBy(p => p.Result, new NumberComparer()).ThenBy(p => p.Bib));
                        _listFinishDataGrid = _tempList;
                        RaisePropertyChanged(() => ListFinishDataGrid);
                        UpdateRank();
                        if (_competitors == 10)
                        {
                            RaceIsOverMethod();
                        }

                    }));
                    


                }
                
            }
        }

       /// <summary>
       /// RaceIsOverMethod : method to update UI and stop simulator
       /// </summary>
        private void RaceIsOverMethod()
        {
            if (_isIndivStartChecked)
            {
                _listFinishDataGrid = _tempList;
                UpdateRank();
            }
            RaisePropertyChanged(() => ListFinishDataGrid);
            _simulator.Stop();
            _raceUpdate = _currentLanguage == "Fr" ? " la Course est terminée" : " the Race is over";
            RaisePropertyChanged(() => RaceUpdate);
            _editButtonIsVisible = true;
            RaisePropertyChanged(() => EditButtonIsVisible);
            _buttonSaveXmlEnable = true;
            RaisePropertyChanged(() => ButtonSaveXmlEnable);
            _isRestartEnable = true;
            RaisePropertyChanged(() => IsRestartEnable);

        }
        /// <summary>
        /// CompEditedMethod : Method to receive edited competitor and refresh UI
        /// </summary>
        /// <param name="obj"></param>
        private void CompEditedMethod(NotificationMessage<RaceResult> obj)
        {
            if (obj.Notification == "CompetitorEdited")
            {
                _isEditViewIsVisible = false;
                 RaisePropertyChanged(() => IsEditViewIsVisible);
                _mainCanvasIsEnable = true;
                RaisePropertyChanged(() => MainCanvasIsEnable);
                var compDisqualified = obj.Content;
                compDisqualified.Rank = "--";
                _listFinishDataGrid.Remove(compDisqualified);
                 UpdateRank();
                _listDisqualified.Add(compDisqualified);
                if (_listDisqualified.Count > 0)
                {
                    _listDisqualified = new ObservableCollection<RaceResult>(_listDisqualified.OrderBy(p => p.Result, new CustomSorter(StringComparer.CurrentCulture))); ;
                }
                RaisePropertyChanged(() => ListDisqualified);
                RaisePropertyChanged(() => ListFinishDataGrid);
            }
           
        }
        /// <summary>
        /// SaveToXmlMethod : Method to save race data in xml file
        /// </summary>
        private void SaveToXmlMethod()
        {
            _listForXml = new ObservableCollection<RaceResult>();
            foreach (var raceResult in _listFinishDataGrid)
            {
                _listForXml.Add(raceResult);
            }
            string path = "";
            SaveFileDialog sd = new SaveFileDialog();
            sd.Filter = "XML-File | *.xml";
            sd.DefaultExt = ".xml";
            if (sd.ShowDialog() == true)
            {
                var fileName = sd.FileName;
                path = Path.GetFullPath(fileName);
                var serializer = new XmlSerializer(typeof(ObservableCollection<RaceResult>));
                using (TextWriter writer = new StreamWriter(path))
                {

                    if (_listDisqualified.Count > 0)
                    {
                        foreach (var raceResult in _listDisqualified)
                        {
                            _listForXml.Add(raceResult);
                        }

                    }
                    serializer.Serialize(writer, _listForXml);
                }
            }
           
           
        }

        /// <summary>
        /// UpdateRank : Method to update ranking after competitor modification
        /// </summary>

        private void UpdateRank()
        {
            var list = new ObservableCollection<RaceResult>();
            foreach (var raceResult in _listFinishDataGrid)
            {
                raceResult.Rank = (_listFinishDataGrid.IndexOf(raceResult) + 1).ToString();
                list.Add(raceResult);
            }
            _listFinishDataGrid = list;
            RaisePropertyChanged(() => ListFinishDataGrid);

        }
        #endregion


        #region IDisposable

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _simulator.RaceCompleted -= SimulatorOnRaceCompleted;
                _simulator.RaceStarted -= SimulatorOnRaceStarted;
                _simulator.CompetitorChanged -= SimulatorOnCompetitorChanged;
                MessengerInstance.Unregister<NotificationMessage<RaceResult>>(this, CompEditedMethod);
                MessengerInstance.Unregister<NotificationMessage>(this, GetMessagesFromOthersVm);

            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~MainViewModel()
        {
            Dispose(false);
        }

        #endregion
    }
}