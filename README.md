# RaceSimulator

RaceSimulator a is race simulation based on the SwissTiming.AcquisitionSimulator library. 
You can simulate races with a provided athletes list in two modes (MassStart and individualStart) then you can watch in live the results.
At the end of the race you can export the result in a Xml file.
A switch language feature is available (English and French)
# Prerequisites

The application works with the .net framework 4.0

# Installing

1.Unzip the file
2.Go to installation folder then click on "setup.exe"
3.A message will appear : "Publisher cannot be verified" ==> Click "Install"
4.The application will start directly after the end of installation process

# Technical brief

.The application is coded with Wpf 4.0 technology and Visual Studio 2015 Enterprise + Resharper.
.The solution is implemented with MVVM pattern, to facilitate this implementation i use the package Mvvm Light by Galasoft.
.List of required librairies :
		.Custom frameworks :
				- Galasoft.MvvmLight.Extras.WPF4
				- Galasoft.MvvmLight.WPF4                 ==> 3 librairies contained in the nuget package (PM> Install-Package MvvmLight -Version 4.2.30)
				- Microsoft.Practices.ServiceLocation
				- SwissTiming.Timing.AcquisitionSimulator ==> library provided by SwissTiming
			
		
# Authors

Lucas Girard

# Licence

No licence for this project
